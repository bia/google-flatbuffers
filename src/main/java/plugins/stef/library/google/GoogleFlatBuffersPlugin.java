package plugins.stef.library.google;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Google FlatBuffers library.
 * 
 * @author Stephane Dallongeville
 */
public class GoogleFlatBuffersPlugin extends Plugin implements PluginLibrary
{
    //
}
